﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLibrary1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1 {

        public static BookManager bookManager = new BookManager();
        public static UserManager userManager = new UserManager();

        public Service1(){
            
            if (!(bookManager.bookList.Any())) {
                for (int i = 0; i < 10; i++) {
                    bookManager.bookList.Add(new Book(i, "Book " + i, "Author"));
                }
            }
        }

        public List<BookInfo> listBorrowedBooks() {
            
            List<BookInfo> borrowedBooksList = new List<BookInfo>();
            foreach (Book b in bookManager.bookList) {
                if (b.status.isBorrowed) {
                    borrowedBooksList.Add(new BookInfo(b));
                }
            }
            return borrowedBooksList;
        }

        public List<BookInfo> listAvailableBooks() {

            List<BookInfo> availableBooksList = new List<BookInfo>();
            foreach (Book b in bookManager.bookList) {
                if (!b.status.isBorrowed) {
                    availableBooksList.Add(new BookInfo(b));
                }
            }
            return availableBooksList;
        }

        public List<BookInfo> getBorrowedBook(int userID) {
            
            List<BookInfo> listOfUserBooks = new List<BookInfo>();
            foreach (Book b in bookManager.bookList) {
                if (b.status.isBorrowed && b.status.userId.Equals(userID)) {
                    listOfUserBooks.Add(new BookInfo(b));
                }
            }
            return listOfUserBooks;
        }

        public BookInfo getBookInfo(int bookID) {

            foreach (Book b in bookManager.bookList) {
                if (b.id.Equals(bookID)) {
                    return new BookInfo(b);
                }
            }

            throw new FaultException<BookIdMissing>(new BookIdMissing(), "");
        }

        public string bookStatus(int bookID) {

              foreach (Book b in bookManager.bookList) {
                if (b.id.Equals(bookID)) {
                    if (b.status.isBorrowed) {
                        return "Book unavaiable";
                    }
                    else {
                        return "Book available"; 
                    } 
                }
            }

            throw new FaultException<BookIdMissing>(new BookIdMissing(), "");
        }

        public Status borrowBook(int userID, int bookID) {

            bool validUser = false;

            foreach (User u in userManager.userList) {
                if (u.id.Equals(userID)) {
                    validUser = true;
                }
            }

            if (!validUser) {
                throw new FaultException<UserIdMissing>(new UserIdMissing(), "");
            }

            foreach(Book b in bookManager.bookList){
                if(b.id.Equals(bookID) && !b.status.isBorrowed){
                    b.status.isBorrowed = true;
                    b.status.userId = userID;
                    return b.status;
                }
            }

             throw new FaultException<BookIdMissing>(new BookIdMissing(), "");
        }

        public void returnBook(int userID, int bookID) {

            bool validUser = false;

            foreach (User u in userManager.userList) {
                if (u.id.Equals(userID)) {
                    validUser = true;
                }
            }

            if (!validUser) {
                throw new FaultException<UserIdMissing>(new UserIdMissing(), "");
            }

            foreach(Book b in bookManager.bookList){
                if(b.id.Equals(bookID)){
                    if (b.status.isBorrowed && b.status.userId.Equals(userID)) {
                        b.status.isBorrowed = false;
                        b.status.userId = 0;
                        return;
                    }
                    else {
                        throw new FaultException<ReturnFault>(new ReturnFault(), "");
                    }
                    
                } 
            }

             throw new FaultException<BookIdMissing>(new BookIdMissing(), "");
        }

        public int login(string _login, string _password) {

            foreach (User u in userManager.userList) {
                if(u.login.Equals(_login) && u.password.Equals(_password)){
                    return u.id;
                }
            }

            int _id = userManager.userList.Count + 1;
            userManager.userList.Add(new User(_id, _login, _password));
            return _id;
        }

        public List<string> getUser() {
            List<string> usersTemp = new List<string>();
            foreach (User u in userManager.userList) {
                usersTemp.Add(u.login);
            }
            return usersTemp;
        }

    }
}
