﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLibrary1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1{

        [OperationContract]
        List<BookInfo> listBorrowedBooks();

        [OperationContract]
        List<BookInfo> listAvailableBooks();

        [OperationContract]
        [FaultContract(typeof(UserIdMissing))]
        List<BookInfo> getBorrowedBook(int userID);

        [OperationContract]
        [FaultContract(typeof(BookIdMissing))]
        BookInfo getBookInfo(int bookID);

        [OperationContract]
        [FaultContract(typeof(BookIdMissing))]
        string bookStatus(int bookID);

        [OperationContract]
        [FaultContract(typeof(BookIdMissing))]
        [FaultContract(typeof(UserIdMissing))]
        Status borrowBook(int userID, int bookID);

        [OperationContract]
        [FaultContract(typeof(BookIdMissing))]
        [FaultContract(typeof(UserIdMissing))]
        [FaultContract(typeof(ReturnFault))]
        void returnBook(int userID, int bookID);

        [OperationContract]
        int login(string login, string password);

        [OperationContract]
        List<String> getUser();

        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "WcfServiceLibrary1.ContractType".
    
    [DataContract]
    public class UserIdMissing {
        public UserIdMissing() { }
    }

    [DataContract]
    public class BookIdMissing {
        public BookIdMissing() { }
    }

    [DataContract]
    public class ReturnFault {
        public ReturnFault() { }
    }

    [DataContract]
    public class Book {
        
        [DataMember]
        public int id;
        
        [DataMember]
        public string title;
        
        [DataMember]
        public string author;

        [DataMember]
        public Status status;

        public Book(int _id, string _title, string _author) {
            this.id = _id;
            this.title = _title;
            this.author = _author;
            this.status = new Status();
        }
    }

    [DataContract]
    public class BookManager {
          
        [DataMember]
        public List<Book> bookList = new List<Book>();
     }

    [DataContract]
    public class BookInfo {

        [DataMember]
        public string bookInfo;

        public BookInfo(Book book){
            bookInfo = "Book id = " + book.id + ", Title: " + book.title + ", Author: " + book.author;  
        }

        public override string ToString(){
 	        return bookInfo.ToString();
        }
    }
       

    [DataContract]
    public class Status {
        
        [DataMember]
        public int userId;

        [DataMember]
        public bool isBorrowed;

        public Status(){
            this.isBorrowed = false;
        }

        public override string ToString(){
            return isBorrowed ? "Book unavailable, " + userId + " has borrowed it." : "Borrowed book from the library, have a good read!"; 
        }
    }

    [DataContract]
    public class User {

        [DataMember]
        public int id;

        [DataMember]
        public string login;

        [DataMember]
        public string password;
        
        public User(int _id, string _login, string _password){
            this.id = _id;
            this.login = _login;
            this.password = _password;
        }
    }

    [DataContract]
    public class UserManager {
        public List<User> userList = new List<User>();
    }
}
