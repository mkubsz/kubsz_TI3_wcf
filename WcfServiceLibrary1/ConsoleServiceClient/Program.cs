﻿using ConsoleServiceClient.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleServiceClient {
    class Program {
        int userId = 0;
        IService1 service = null;

        public Program() {
            var serviceBinding = new BasicHttpBinding();
            var sericeEndpoint = new EndpointAddress("http://localhost:8733/Design_Time_Addresses/WcfServiceLibrary1/Service1/");
            var serviceChannelFactory = new ChannelFactory<IService1>(serviceBinding, sericeEndpoint);
            service = serviceChannelFactory.CreateChannel();
        }

        ~Program() {
            if (service != null) {
                ((ICommunicationObject)service).Close();
            }
        }

        public void showMenu() {

            Console.WriteLine("login");
            Console.WriteLine("available");
            Console.WriteLine("unavailable");
            Console.WriteLine("borrow");
            Console.WriteLine("return");
            Console.WriteLine("myBooks");
            Console.WriteLine("about");
            Console.WriteLine("status");
            Console.WriteLine("quit");
        }

        public void chooseOption() {
            
            String userInput = "";
            do {
                Console.WriteLine("==>");
                userInput = Console.ReadLine();

                switch (userInput) {

                    case "login":
                        Console.WriteLine("Enter login:");
                        string login = Console.ReadLine();
                        Console.WriteLine("Enter password:");
                        string password = Console.ReadLine();
                        userId = service.login(login, password);
                        break;

                    case "available":
                        if (userId == 0) {
                            Console.WriteLine("Login first");
                            continue;
                        }
                        List<BookInfo> availableBooksList = service.listAvailableBooks();
                        if (availableBooksList.Count == 0)
                            Console.WriteLine("All books currently unavailable");
                        else
                            Console.WriteLine("Available books:");
                        foreach (BookInfo b in availableBooksList)
                            Console.WriteLine(b.bookInfo.ToString());
                        break;

                    case "borrow":
                        if (userId == 0) {
                            Console.WriteLine("Login first");
                            continue;
                        }
                        try {
                            Console.WriteLine("Enter bookID:");
                            int bookId = Convert.ToInt32(Console.ReadLine());
                            service.borrowBook(userId, bookId);
                        }
                        catch (FaultException<BookIdMissing>) {
                            Console.WriteLine("Book of given ID doesn't exist, or this book is currently unavailable!");
                        }
                        catch (FaultException<UserIdMissing>) {
                            Console.WriteLine("User of given ID doesn't exist!");
                        }
                        break;

                    case "return":
                        if (userId == 0) {
                            Console.WriteLine("Login first");
                            continue;
                        }
                        try {
                            Console.WriteLine("Enter bookID:");
                            int bookId = Convert.ToInt32(Console.ReadLine());
                            service.returnBook(userId, bookId);
                        }
                        catch (FaultException<BookIdMissing>) {
                            Console.WriteLine("Book of given ID doesn't exist!");
                        }
                        catch (FaultException<UserIdMissing>) {
                            Console.WriteLine("User of given ID doesn't exist!");
                        }
                        catch (FaultException<ReturnFault>) {
                            Console.WriteLine("You can not return that book!");
                        }
                        break;

                    case "unavailable":
                        if (userId == 0) {
                            Console.WriteLine("Login first");
                            continue;
                        }
                        List<BookInfo> unavailableBooksList = service.listBorrowedBooks();
                        if (unavailableBooksList.Count == 0)
                            Console.WriteLine("No books are borrowed");
                        else
                            Console.WriteLine("Borrowed books:");
                        foreach (BookInfo b in unavailableBooksList)
                            Console.WriteLine(b.bookInfo);
                        break;

                    case "myBooks":
                        if (userId == 0) {
                            Console.WriteLine("Login first");
                            continue;
                        }
                        try {
                            List<BookInfo> myBooks = service.getBorrowedBook(userId);
                            if (myBooks.Count == 0)
                                Console.WriteLine("You haven't borrowed any books yet");
                            else
                                Console.WriteLine("Your books:");
                            foreach (BookInfo b in myBooks)
                                Console.WriteLine(b.bookInfo);
                        }
                        catch (FaultException<BookIdMissing>) {
                            Console.WriteLine("Book of given ID doesn't exist!");
                        }
                        break;

                    case "about":
                        if (userId == 0) {
                            Console.WriteLine("Login first");
                            continue;
                        }
                        try {
                            Console.WriteLine("Enter bookID:");
                            int bookId = Convert.ToInt32(Console.ReadLine());
                            BookInfo inf = service.getBookInfo(bookId);
                            Console.WriteLine(inf.bookInfo);
                        }
                        catch (FaultException<BookIdMissing>) {
                            Console.WriteLine("Book of given ID doesn't exist!");
                        }
                        break;

                    case "status":
                        if (userId == 0) {
                            Console.WriteLine("Login first");
                            continue;
                        }
                        try {
                            Console.WriteLine("Enter bookID:");
                            int bookId = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine(service.bookStatus(bookId).ToString());
                        }
                        catch (FaultException<BookIdMissing>) {
                            Console.WriteLine("Book of given ID doesn't exist!");
                        }
                        break;

                    default:
                        Console.WriteLine("Unknown command");
                        break;

                }
            } while (!userInput.Equals("quit"));
        }
                /*
                if (userInput.Equals("login")) {
                    Console.WriteLine("Enter login:");
                    string login = Console.ReadLine();
                    Console.WriteLine("Enter password:");
                    string password = Console.ReadLine();
                    userId = service.login(login, password);
                }
                else if (userInput.Equals("available")) {
                    if (userId == 0) {
                        Console.WriteLine("Nie jesteś zalogowany.");
                        continue;
                    }
                    List<BookInfo> bookList = service.listAvailableBooks();
                    if (bookList.Count == 0)
                        Console.WriteLine("Nie ma dostępnych książek.");
                    else
                        Console.WriteLine("Dostępne książki:");
                    foreach (BookInfo b in bookList)
                        Console.WriteLine(b.bookInfo.ToString());
                }
                else if (userInput.Equals("wypozyczone")) {
                    if (userId == 0) {
                        Console.WriteLine("Nie jesteś zalogowany.");
                        continue;
                    }
                    List<BookInfo> books = service.listBorrowedBooks();
                    if (books.Count == 0)
                        Console.WriteLine("Nie ma wypożyczonych książek.");
                    else
                        Console.WriteLine("Wypożyczone książki:");
                    foreach (BookInfo b in books)
                        Console.WriteLine(b.bookInfo);
                }
                else if (userInput.Equals("wypozyczoneMoje")) {
                    if (userId == 0) {
                        Console.WriteLine("Nie jesteś zalogowany.");
                        continue;
                    }
                    try {
                        List<BookInfo> books = service.getBorrowedBook(userId);
                        if (books.Count == 0)
                            Console.WriteLine("Nie masz wypożyczonych książek.");
                        else
                            Console.WriteLine("Wypożyczone książki:");
                        foreach (BookInfo b in books)
                            Console.WriteLine(b.bookInfo);
                    }
                    catch (FaultException<BookIdMissing>) {
                        Console.WriteLine("Podano złe ID książki");
                    }
                }
                else if (userInput.Equals("info")) {
                    if (userId == 0) {
                        Console.WriteLine("Nie jesteś zalogowany.");
                        continue;
                    }
                    try {
                        Console.WriteLine("Podaj ID książki:");
                        int bookId = Convert.ToInt32(Console.ReadLine());
                        BookInfo inf = service.getBookInfo(bookId);
                        Console.WriteLine(inf.bookInfo);
                    }
                    catch (FaultException<BookIdMissing>) {
                        Console.WriteLine("Podano złe ID książki");
                    }
                }
                else if (userInput.Equals("status")) {
                    if (userId == 0) {
                        Console.WriteLine("Nie jesteś zalogowany.");
                        continue;
                    }
                    try {
                        Console.WriteLine("Podaj ID książki:");
                        int bookId = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine(service.bookStatus(bookId).ToString());
                    }
                    catch (FaultException<BookIdMissing>) {
                        Console.WriteLine("Podano złe ID książki");
                    }
                }
                else if (userInput.Equals("wypozycz")) {
                    if (userId == 0) {
                        Console.WriteLine("Nie jesteś zalogowany.");
                        continue;
                    }
                    try {
                        Console.WriteLine("Podaj ID książki:");
                        int bookId = Convert.ToInt32(Console.ReadLine());
                        service.borrowBook(userId, bookId);
                    }
                    catch (FaultException<BookIdMissing>) {
                        Console.WriteLine("Podano złe ID książki");
                    }
                    catch (FaultException<UserIdMissing>) {
                        Console.WriteLine("Użytkownik o tym ID nie istnieje");
                    }
                }
                else if (userInput.Equals("oddaj")) {
                    if (userId == 0) {
                        Console.WriteLine("Nie jesteś zalogowany.");
                        continue;
                    }
                    try {
                        Console.WriteLine("Podaj ID książki:");
                        int bookId = Convert.ToInt32(Console.ReadLine());
                        service.returnBook(userId, bookId);
                    }
                    catch (FaultException<BookIdMissing>) {
                        Console.WriteLine("Podano złe ID książki");
                    }
                    catch (FaultException<UserIdMissing>) {
                        Console.WriteLine("Użytkownik o tym ID nie istnieje");
                    }
                    catch (FaultException<ReturnFault>) {
                        Console.WriteLine("Użytkownik o tym ID nie posiada takiej książki.");
                    }
                }
                else {
                    Console.WriteLine("Nie poprawne polecenie");
                }
            } while (!userInput.Equals("quit"));
            Console.WriteLine("end");
            Console.ReadLine();
        }
        */
        static void Main(string[] args) {
            Program program = new Program();
            program.showMenu();
            program.chooseOption();
        }
    }
}
